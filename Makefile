SRC_DIR := $(PWD)/src
TARGET_DIR := $(HOME)

SRC_FILES := $(shell ls -1A $(SRC_DIR))
STOW := stow -t $(TARGET_DIR)

install: force-purge stow-install
reinstall: force-purge stow-reinstall
uninstall: stow-uninstall
check: stow-check
update: reinstall

stow-install:
	@echo "# Linking Stowed Files"
	@$(STOW) -S src

stow-reinstall:
	@echo "# Relinking Stowed Files"
	@$(STOW) -R src

stow-uninstall:
	@echo "# Unlinking Stowed Files"
	@$(STOW) -D src

stow-check:
	@$(STOW) -n -S src

ls:
	@ls -CA $(SRC_DIR)

force-purge:
	@echo "# Removing any conflicting files using 'make check'"
	@$(MAKE) check 2>&1 \
		| grep -e 'existing target is neither a link nor a directory: ' \
		| awk -F ':' '{print $$2}' \
		| xargs -I% rm -vrf $(TARGET_DIR)/%
