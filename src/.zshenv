#                            $HOME/.zshenv
#                           ---------------
# Sourced on ALL invocations of the shell (unless 'zsh -f' is used)
#
# It should:
# - Set global environment vars like PATH, EDITOR, PAGER, BROWSER, etc.
# - NEVER require a TTY
# - Generate ABSOLUTELY NO output
#

# Load ~/.zprofile if it exists, otherwise load ~/.profile. If neither exist,
# alert the user that there is a problem.
if [[ -f "$HOME/.zprofile" ]]; then
  source "$HOME/.zprofile"

elif [[ -f "$HOME/.profile" ]]; then
  source "$HOME/.profile"

fi
