#!/bin/bash
# toolbox - bring your tools with you.
#
# toolbox is a small script that launches a container to let you bring in your
# favorite debugging or admin tools.
#
# ## Usage
# ```
# $ /usr/bin/toolbox
# Spawning container core-fedora-latest on /var/lib/toolbox/core-fedora-latest.
# Press ^] three times within 1s to kill container.
# [root@localhost ~]# dnf -y install tcpdump
# ...
# [root@localhost ~]# tcpdump -i ens3
# tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
# listening on ens3, link-type EN10MB (Ethernet), capture size 65535 bytes
# ```
#
# ## Advanced Usage
#
# ### Use a custom image
#
# toolbox uses a Fedora-based userspace environment by default, but this can be changed to any Docker image. Simply override environment variables in `$HOME/.toolboxrc`:
#
# ```
# core@core-01 ~ $ cat ~/.toolboxrc
# TOOLBOX_DOCKER_IMAGE=ubuntu-debootstrap
# TOOLBOX_DOCKER_TAG=14.04
# core@core-01 ~ $ toolbox
# Spawning container core-ubuntu-debootstrap-14.04 on /var/lib/toolbox/core-ubuntu-debootstrap-14.04.
# Press ^] three times within 1s to kill container.
# root@core-01:~# apt-get update && apt-get install tcpdump
# ```
#
# ### Automatically enter toolbox on login
#
# Set an `/etc/passwd` entry for one of the users to `/usr/bin/toolbox`:
#
# ```sh
# useradd bob -m -p '*' -s /usr/bin/toolbox -U -G sudo,docker,rkt
# ```
#
# Now when SSHing into the system as that user, toolbox will automatically be started:
# ```
# $ ssh bob@hostname.example.com
# Container Linux by CoreOS alpha (1284.0.0)
# ...
# Spawning container bob-fedora-latest on /var/lib/toolbox/bob-fedora-latest.
# Press ^] three times within 1s to kill container.
# [root@localhost ~]# dnf -y install emacs-nox
# ...
# [root@localhost ~]# emacs /media/root/etc/systemd/system/docker.service
# ```
#
# ## Bugs
# Please use the [CoreOS issue tracker][bugs] to report all bugs, issues, and feature requests.

# [bugs]: https://github.com/coreos/bugs/issues/new?labels=component/toolbox

set -e
set -o pipefail

machine=$(uname -m)

case ${machine} in
  aarch64 )
    TOOLBOX_DOCKER_IMAGE=aarch64/fedora
    TOOLBOX_DOCKER_TAG=latest
    ;;
  x86_64 )
    TOOLBOX_DOCKER_IMAGE=fedora
    TOOLBOX_DOCKER_TAG=latest
    ;;
  * )
    echo "Warning: Unknown machine type ${machine}" >&2
    ;;
esac

TOOLBOX_USER=root
TOOLBOX_DIRECTORY="/var/lib/toolbox"
TOOLBOX_BIND="--bind=/:/media/root --bind=/usr:/media/root/usr --bind=/run:/media/root/run"
# Ex: "--setenv=KEY=VALUE"
TOOLBOX_ENV=""

toolboxrc="${HOME}"/.toolboxrc

# System defaults
if [ -f "/etc/default/toolbox" ]; then
  source "/etc/default/toolbox"
fi

# User overrides
if [ -f "${toolboxrc}" ]; then
  source "${toolboxrc}"
fi

if [[ -n "${TOOLBOX_DOCKER_IMAGE}" ]] && [[ -n "${TOOLBOX_DOCKER_TAG}" ]]; then
  TOOLBOX_NAME=${TOOLBOX_DOCKER_IMAGE}-${TOOLBOX_DOCKER_TAG}
  have_docker_image="y"
fi

machinename=$(echo "${USER}-${TOOLBOX_NAME}" | sed -r 's/[^a-zA-Z0-9_.-]/_/g')
machinepath="${TOOLBOX_DIRECTORY}/${machinename}"
osrelease="${machinepath}/etc/os-release"
if [ ! -f "${osrelease}" ] || systemctl is-failed -q "${machinename}" ; then
  sudo mkdir -p "${machinepath}"
  sudo chown "${USER}:" "${machinepath}"

  if [[ -n "${have_docker_image}" ]]; then
    riid=$(sudo --preserve-env rkt --insecure-options=image fetch "docker://${TOOLBOX_DOCKER_IMAGE}:${TOOLBOX_DOCKER_TAG}")
    sudo --preserve-env rkt image extract --overwrite --rootfs-only "${riid}" "${machinepath}"
    sudo --preserve-env rkt image rm "${riid}"
  elif [[ -n "${TOOLBOX_DOCKER_ARCHIVE}" ]]; then
    tmpdir=$(mktemp -d -p /var/tmp/)
    trap "sudo rm -rf ${tmpdir}" EXIT PIPE
    wget -O- "${TOOLBOX_DOCKER_ARCHIVE}" | xz -cd | tar -C ${tmpdir} -xf -
    layer=$(find ${tmpdir} -name layer.tar -type f)
    sudo tar -C ${machinepath} -xf ${layer}
    trap - EXIT PIPE
    sudo rm -rf ${tmpdir}
  else
    echo "Error: No toolbox filesystem specified." >&2
    exit 1
  fi
  sudo touch "${osrelease}"
fi

# Special case for when SSH tries to pass a shell command with -c
if [ "x${1-}" == x-c ]; then
  set /bin/sh "$@"
fi

sudo systemd-nspawn \
  --directory="${machinepath}" \
  --capability=all \
  --share-system \
        ${TOOLBOX_BIND} \
        ${TOOLBOX_ENV} \
  --user="${TOOLBOX_USER}" "$@"
