#                               ~/.zshrc
# ---------------------------------------------------------------------
# Prefer making changes to ~/.zshrc.local unless you plan on merging!
try_source(){ [[ ! -f "$1" ]] || source "$1" ;}
try_source "$HOME/.config/shell/load_shell.sh"

for s in $(find "${ZSH_DIR:-$HOME/.config/zsh}/lib/my_configs/" -iname "*.zsh"); do
  try_source "$s"
done

# Fuzzy Finder: Keybinds & Functions.
try_source "$HOME/.fzf.zsh"

# Google Cloud SDK: Update PATH / Command Completion.
try_source "/opt/google-cloud-sdk/path.zsh.inc"
try_source "/opt/google-cloud-sdk/completion.zsh.inc"

# Local ZSH Overrides (SHOULD be last).
try_source "${HOME}/.zshrc.local"
