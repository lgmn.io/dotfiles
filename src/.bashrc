#
# ~/.bashrc
#
[[ $- == *i* ]] || return;

source "$HOME/.config/shell/load_shell.sh"

source_if_exists "$HOME/.bashrc.local"

if [[ $(shell_is_interactive) ]]; then
  source_if_exists "/usr/share/bash-completion/bash_completion"
  source_if_exists "$HOME/.fzf.bash"
fi
