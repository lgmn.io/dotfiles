#!/usr/bin/env python3
import os
import fire
import requests
import json
from slackclient import SlackClient

SLACK_TOKEN = os.environ.get("SLACK_TOKEN", None)

class Slacky(object):

    def __init__(self, slack_token):
        self._slack = SlackClient(token=slack_token)
        self._team = self._slack.api_call('team.info')['team']
        self._team['icon'] = self._team['icon']['image_original']
        self._slack_url = "{domain}.slack.com".format(**self._team)

    def __repr__(self):
        return self._slack_url

    def _post_msg(self, channel, text):
        return self._slack.api_call('chat.postMessage', as_user=True,
                                    channel=channel, text=text)

    def _list_emoji(self):
        return self._slack.api_call('emoji.list')['emoji']

    def _print_channel_li(self, chan):
        arc = ""
        sym = ""

        if chan.get("is_archived", False):
            arc = " [A] "

        if chan.get("is_channel", False):
            sym = "#"

        fmt = "{sym}{name_normalized}{arc}"

        return fmt.format(sym=sym, arc=arc, **chan)

    def _print_user_li(self, user):

        if user.get("is_primary_owner", False):
            role_decorator = "~"
        elif user.get("is_owner", False):
            role_decorator = "&"
        elif user.get("is_admin", False):
            role_decorator = "@"
        elif user.get("is_bot", False):
            role_decorator = "+"
        elif user.get("is_deleted", False):
            role_decorator = "-"
        else:
            role_decorator = ""

        fmt = "{sym}{name}"

        return fmt.format(sym=role_decorator, **user)

    def say(self, channel, message):
        """Posts a simple message to slack."""
        return self._post_msg(channel, message)

    def list_emoji(self):
        """Lists all emoji and their URLs."""
        return self._list_emoji()

    def list_channels(self):
        """Lists all channels."""
        channels = self._slack.api_call('channels.list')['channels']
        return list(map(self._print_channel_li, channels))

    def list_users(self):
        """Lists all users."""
        users = self._slack.api_call('users.list')['members']
        return list(map(self._print_user_li, users))

    def download_emoji(self, emoji):
        """Downloads a given emoji."""
        url = self._list_emoji()[emoji]
        ext = url.split('.')[-1]
        filename = "{emoji}.{ext}".format(emoji=emoji, ext=ext)

        print("Downloading {f}".format(f=filename))
        r = requests.get(url)

        with open(filename, "wb") as f:
            f.write(r.content)

        return r.status_code

    def download_all_emoji(self):
        """Downloads all emoji from Slack."""
        emoji = self._list_emoji()
        results = dict()

        for name, url in emoji.items():
            if url.startswith('alias:'):
                continue
            results[name] = self.download_emoji(name)

        return results

    def generate_emojipack(self):
        """Generates an emojipacks-compatible list of all emoji."""
        emoji = self.list_emoji()
        out = []

        out.append("title: {domain}".format(**self._team))
        out.append("emojis:")
        for k, v in emoji.items():
            if v.startswith('alias:'):
                continue
            i = dict()
            i['name'] = k
            i['src'] = v
            out.append("  - {}".format(json.dumps(i)))
        return out

    def team_info(self):
        """Lists all global team information."""
        return self._team


def main():
    fire.Fire(Slacky(SLACK_TOKEN), name='slacky')


if __name__ == '__main__':
    main()
