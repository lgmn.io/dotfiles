#!/bin/bash
set -euo posix
readonly RB_EXEC=${1:-irb}
readonly DEBUG=${DEBUG:-0}
ensure_gem_installed(){ gem list --silent -i "${1}" || gem install "${1}" ;}

case "$RB_EXEC" in
  pry)
    rb_args+="--simple-prompt"
    ensure_gem_installed "pry"
    ensure_gem_installed "pry-syntax-hacks"
    ensure_gem_installed "pry-state"
    ;;
  irb)
    [[ $DEBUG -eq 0 ]] || rb_args+=" -d"
    rb_args+="  -r irbtools/more"
    rb_args+="  --prompt simple"
    rb_args+="  -m"                     #Preload Math Stuff.
    rb_args+="  --readline"             #Enable Readline
    rb_args+="  --inspect"              #Inspect all output.
    rb_args+="  --echo"                 #Echo all return values.
    ensure_gem_installed "irb"
    ensure_gem_installed "irbtools"
    ensure_gem_installed "irbtools-more"
    ;;
  *) exit -1 ;;
esac

# Start the IRB shell.
[[ $DEBUG -eq 0 ]] || set -x
exec $RB_EXEC ${rb_args[@]}
