" Load standard YAML syntax.
" source <sfile>:p:h/include/yaml.vim

" Match language specific keywords
syntax match k8sApiVersions contained 'v1'
syntax match k8sApiVersions contained 'extensions\/v1beta1'
syntax match k8sApiVersions contained 'rbac\.authorization\.k8s\.io\/v1beta1'

syntax match k8sKinds contained 'ClusterRole'
syntax match k8sKinds contained 'ClusterRoleBinding'
syntax match k8sKinds contained 'ConfigMap'
syntax match k8sKinds contained 'CronJob'
syntax match k8sKinds contained 'Deployment'
syntax match k8sKinds contained 'Ingress'
syntax match k8sKinds contained 'Pod'
syntax match k8sKinds contained 'ReplicaSet'
syntax match k8sKinds contained 'ReplicationController'
syntax match k8sKinds contained 'ScheduledJob'
syntax match k8sKinds contained 'Secret'
syntax match k8sKinds contained 'Service'
syntax match k8sKinds contained 'ServiceAccount'

syntax match k8sServiceTypes contained 'NodePort'
syntax match k8sServiceTypes contained 'ClusterIP'
syntax match k8sServiceTypes contained 'LoadBalancer'
syntax match k8sServiceTypes contained 'RollingUpdate'

syntax region k8sApiVersion   start='^\s*apiVersion:\s\+' end='$'  contains=k8sApiVersions
syntax region k8sKind         start='^\s*kind:\s\+' end='$'        contains=k8sKinds
syntax region k8sServiceType  start='^\s*type:\s\+' end='$'        contains=k8sServiceTypes
syntax region k8sMetadata     start='^\s*metadata:' end='$'        fold
syntax region k8sData         start='^\s*data:' end='$'            fold
syntax region k8sTemplate     start='^\s*template:' end='$'        fold
syntax region k8sSpec         start='^\s*spec:' end='$'            fold

" Special resource declarations
hi def link  k8sApiVersion   Keyword
hi def link  k8sKind         Keyword
hi def link  k8sMetadata     Keyword
hi def link  k8sSpec         Keyword
hi def link  k8sData         Keyword
hi def link  k8sTemplate     Keyword
hi def link  k8sServiceType  Keyword

" Set highlights
hi def link  k8sServiceTypes  Special
hi def link  k8sKinds         Special
hi def link  k8sApiVersions   Special
