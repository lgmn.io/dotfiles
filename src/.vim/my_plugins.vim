 call my_config#plugins_begin()

" Plug 'tpope/vim-endwise'                 "Automatic closing brackets. (CONFLICTS w/ ultisnips)
Plug 'aperezdc/vim-template'             "Vim Filetype Templates.
Plug 'godlygeek/tabular'                 "Easier alignment of text into tables.
Plug 'kana/vim-textobj-function'         "Adds functions as text-objects.
Plug 'kana/vim-textobj-indent'           "Adds indentation as a text-object.
Plug 'kana/vim-textobj-user'             "Custom text-object definitions.
Plug 'mhinz/vim-signify'                 "Git Diff in the buffer's gutter.
Plug 'nathanaelkane/vim-indent-guides'   "Indent guides for indenting smarter.
Plug 'tpope/vim-abolish'                 "Easily replace multiple variants of a project.
Plug 'tpope/vim-commentary'              "Toggle comments for text.
Plug 'tpope/vim-dispatch'                "Async background commands.
Plug 'tpope/vim-projectionist'           "Project detection / local settings.
Plug 'tpope/vim-repeat'                  "More commands support repeat ('.')
Plug 'tpope/vim-sleuth'                  "Auto detect indentation.
Plug 'tpope/vim-surround'                "Easily wrap a selection in quotes or parens.
Plug 'tpope/vim-unimpaired'              "Pairs of handy bracket mappings
Plug 'w0rp/ale'                          "Async language agnostic linting.
Plug 'wellle/targets.vim'                "Adds additional text target objects to vim.
Plug 'wincent/pinnacle'                  "Highlight group manipulation for Vim
Plug 'Shougo/junkfile.vim'

" Compatibility
" Plug 'editorconfig/editorconfig-vim'     "Support editorconfig spec.
Plug 'sheerun/vim-polyglot'              "Massive list of syntax plugins.
Plug 'tpope/vim-eunuch'                  "Adds :SudoEdit / :SudoWrite
Plug 'tpope/vim-jdaddy'                  "JSON formatting / text objects

" Sidebars, Preview Windows, Utilities
Plug 'vimwiki/vimwiki'                   "Personal Wiki/Notes
Plug 'tpope/vim-vinegar'                 "Minimalist file-browser (supports NetRW).
Plug 'Shougo/denite.nvim'                "Async Fuzzy Finder Menu.
Plug 'majutsushi/tagbar'                 "Sidebar of symbols using CTags.
Plug 'scrooloose/nerdtree'               "File-browser.
Plug 'skywind3000/quickmenu.vim'         "Customizable sidebar menu.
Plug 'tpope/vim-fugitive'                "Best Git integration
Plug 'bling/vim-airline'                 "Powerline-style Status bar.

" Autocompletion
Plug 'Shougo/neocomplete.vim'
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'Shougo/neco-vim'

" = [ Terraform / HCL ] ===============================================
Plug 'hashivim/vim-terraform'
Plug 'juliosueiras/vim-terraform-completion'

" = [ Ruby ] ==========================================================
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rbenv'                   "Adds RbEnv Support.
Plug 'tpope/vim-endwise'                 "Automatically ends ruby blocks.
Plug 'tpope/vim-bundler'

" = [ Optional Plugins ] ===============================================
if executable('docker')
  Plug 'docker/docker'
endif

if executable('go')
  Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
endif

if executable('rustc')
  Plug 'racer-rust/vim-racer'
endif

call my_config#plugins_end()
