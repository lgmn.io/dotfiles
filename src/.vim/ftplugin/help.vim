nohlsearch
setl nospell
setl nolist
setl textwidth=0
setl tabstop=2
setl shiftwidth=2
setl softtabstop=2
