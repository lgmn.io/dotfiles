IndentGuidesDisable
setl nospell

setl tabstop=2
setl shiftwidth=2
setl softtabstop=2
