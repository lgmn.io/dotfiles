setl comments=s1:##,mb:#,ex:##,:#,b:#
setl commentstring=#%s
setl nowrap
setl makeprg=terraform\ fmt\ -diff
