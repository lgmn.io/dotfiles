" [ NeoComplete ] =====================================================
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length = 1

" Closing the completion popup.
inoremap <expr><BS>     neocomplete#smart_close_popup()."\<BS>"
inoremap <expr><C-h>    neocomplete#smart_close_popup()."\<C-h>"

" Undo previous completion.
inoremap <expr><C-g>    neocomplete#undo_completion()

" Select a completion to insert.
inoremap <expr><C-j>    pumvisible() ? "\<C-n>" : "\<C-j>"
inoremap <expr><C-k>    pumvisible() ? "\<C-p>" : "\<C-k>"
inoremap <expr><TAB>    pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB>  pumvisible() ? "\<C-p>" : "\<S-TAB>"

" Complete whats common between entries.
inoremap <expr><C-l>    neocomplete#complete_common_string()

" Accept a completion and insert it.
function! s:my_cr_function()
  " <CR>: close popup and save indent.
  return (pumvisible() ? "\<C-k>" : "" ) . "\<CR>"
endfunction

inoremap <expr><Space>  pumvisible() ? "\<C-y><Space>" : "\<Space>"
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>

" Remove Info (Preview) window
set completeopt-=preview

" Hide Info (Preview) window after completions
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif

let g:neocomplete#sources#omni#input_patterns.terraform = '[^ *\t"{=$]\w*'
