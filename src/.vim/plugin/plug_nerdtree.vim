" NERDTree
let g:NERDTreeAutoCenter          = 1
let g:NERDTreeAutoDeleteBuffer    = 1
let g:NERDTreeBookmarksSort       = 1
let g:NERDTreeCascadeSingleChild  = 0
let g:NERDTreeChDirMode           = 2
let g:NERDTreeHighlightCursorline = 0
let g:NERDTreeHijackNetrw         = 1
let g:NERDTreeMinimalUI           = 0
let g:NERDTreeQuitOnOpen          = 1
let g:NERDTreeRespectWildIgnore   = 1
let g:NERDTreeShowBookmarks       = 1
let g:NERDTreeShowHidden          = 1
let g:NERDTreeShowLineNumbers     = 0
let g:NERDTreeDirArrowCollapsible = '-'
let g:NERDTreeDirArrowExpandable  = '+'
let g:NERDTreeCreatePrefix        = 'silent keepalt keepjumps'
