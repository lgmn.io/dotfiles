" Netrw (File browser) Configs
" ----------------------------
let g:netrw_altv = 0                              "Open splits to the right.
let g:netrw_keepdir = 0                           "Keep track of remote directory.
let g:netrw_banner = 0                            "Disable annoying banner.
let g:netrw_browse_split = 0                      "Use current window.
let g:netrw_list_hide  = netrw_gitignore#Hide()   "Ignore files in .gitignore.
" let g:netrw_list_hide .= ',\(^\|\s\s\)\zs\.\S\+'  "Hide
let g:netrw_liststyle = 3                       "Use TREE view instead of list.

