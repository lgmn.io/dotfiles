" QuickMenu
let g:quickmenu_options = "HL"
let g:quickmenu_padding_left = 1
let g:quickmenu_padding_right = 1
let g:quickmenu_disable_nofile = 0    "Always open, even if no file is open.

" Menu 0: Default Menu.
call g:quickmenu#current(0)
call g:quickmenu#reset()
call g:quickmenu#header('Menus')
call quickmenu#append("TagBar",           'TagbarToggle', "Open the TagBar (CTags).")
call quickmenu#append("File Menu",        'call quickmenu#toggle(1)', "Open the file menu.")
call quickmenu#append("Git Menu",         'call quickmenu#toggle(2)', "Manage Git operations with ease..")

" Menu 1: File Menu.
call g:quickmenu#current(1)
call g:quickmenu#reset()
call g:quickmenu#header('File Menu')
call quickmenu#append("# Quick Toggles",                   '')
call quickmenu#append("Turn paste %{&paste? 'off':'on'}",  "set paste!", "enable/disable paste mode (:set paste!)")
call quickmenu#append("Turn spell %{&spell? 'off':'on'}",  "set spell!", "enable/disable spell check (:set spell!)")
call quickmenu#append("Function List",                     "TagbarToggle", "Switch Tagbar on/off")

" Menu 2: Git Menu.
call g:quickmenu#current(2)
call g:quickmenu#reset()
call g:quickmenu#header('Git Menu')
call quickmenu#append("amend commit",     'Gcommit --amend', "Amend the previous git commit.")
call quickmenu#append("blame",            'Gblame', "use fugitive's Gblame on current document")
call quickmenu#append("diff",             'Gvdiff', "use fugitive's Gvdiff on current document")
call quickmenu#append("fetch",            'Git fetch --all', "Update all remote references.")
call quickmenu#append("push",             'Gpush origin', "use fugitive's Gpush on current document")
call quickmenu#append("status",           'Gstatus', "use fugitive's Gstatus on current document")

" Menu 3: Wiki Menu.
call g:quickmenu#current(3)
call g:quickmenu#reset()
call g:quickmenu#header('My Notes Menu')
call quickmenu#append("Index",            'tab VimwikiIndex', "Open Vimwiki Index.")
call quickmenu#append("Today",            'tab VimwikiMakeDiaryNote', "Open Today in Vimwiki.")
call quickmenu#append("Yesterday",        'tab VimwikiMakeYesterdayDiaryNote', "Open Yesterday in Vimwiki.")
call quickmenu#append("Daily Index",      'tab VimwikiDiaryIndex', "Open Daily Notes Index in Tab.")
call quickmenu#append("Generate Links",   'VimwikiDiaryGenerateLinks', "Generate links for daily notes.")
call quickmenu#append("Select Wiki",      'VimwikiUISelect', "Open Yesterday in Vimwiki.")

" Menu 4: "IDE" Menu.
call g:quickmenu#current(4)
call g:quickmenu#reset()
call g:quickmenu#header('"IDE" Menu')
call quickmenu#append("GoToDeclaration",  'GoToDeclaration')

" Menu 98: Help Menu.
call g:quickmenu#current(98)
call g:quickmenu#reset()
call g:quickmenu#header('Help Docs')
call quickmenu#append('completion',       'tab help youcompleteme', '')
call quickmenu#append('git',              'tab help fugitive', '')
call quickmenu#append('help',             'tab help help', '')
call quickmenu#append('json',             'tab help jdaddy', '')
call quickmenu#append('linter',           'tab help ale', '')
call quickmenu#append('rust',             'tab help ft_rust', '')
call quickmenu#append('snippets',         'tab help UltiSnips', '')
call quickmenu#append('surround',         'tab help surround', '')
call quickmenu#append('wiki',             'tab help vimwiki', '')

" Menu 99: Vim Menu.
call g:quickmenu#current(99)
call g:quickmenu#reset()
call g:quickmenu#header('Vim Menu')
call quickmenu#append("# Configs",        '')
call quickmenu#append("~/.vimrc",         'exec "tabedit" . expand($MYVIMRC)', "Edit current Vimrc.")
call quickmenu#append("Bookmarks",        'exec "tabedit" . g:NERDTreeBookmarksFile', "Edit NERDTree Bookmarks.")
call quickmenu#append("Reload Config",    'exec "source" . expand($MYVIMRC)', "Reload vim configuration.")
call quickmenu#append("# Plugins",        '')
call quickmenu#append("Install",          'PlugInstall', "Install all plugins listed in Vimrc.")
call quickmenu#append("Update",           'PlugUpdate', "Update all plugins listed in Vimrc.")
call quickmenu#append("Clean",            'PlugClean', "Clean all plugins not listed in Vimrc.")

