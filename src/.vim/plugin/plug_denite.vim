" == [ Keybindings ] ==================================================
call denite#custom#map('insert', '<C-j>', '<denite:move_to_next_line>', 'noremap')
call denite#custom#map('insert', '<C-k>', '<denite:move_to_previous_line>', 'noremap')

" == [ General Preferences ] ==========================================

" Change default prompt
call denite#custom#option('default', {
      \ 'prompt': '>',
      \ 'updatetime': 50,
      \ 'short_source_names': v:true,
      \ 'resume': v:false,
      \ 'auto_accel': v:false,
      \ 'auto_highlight': v:false,
      \ 'auto_preview': v:false,
      \ 'auto_resize': v:true,
      \ 'highlight_matched_range': 'Identifier',
      \ 'highlight_matched_char': 'Special',
      \ 'highlight_mode_insert': 'Directory',
      \ 'highlight_mode_normal': 'Directory',
      \ 'highlight_preview_line': 'Special',
      \ 'cursor_wrap': v:false,
      \ 'quit': v:true,
      \ 'refresh': v:false,
      \ 'smartcase': v:true,
      \ 'statusline': v:false,
      \ 'split': 'horizontal',
      \ 'matchers': 'matcher_fuzzy',
      \ 'sorters': 'sorter_sublime'
      \ })

" Always ignore the following entries.
call denite#custom#filter('matcher_ignore_globs', 'ignore_globs', [
      \ '*.swo',
      \ '*.swp',
      \ '*.o',
      \ '*.a',
      \ '*.so',
      \ '*~',
      \ '.git/',
      \ '.ropeproject/',
      \ '.venv/',
      \ '__pycache__/',
      \ ])

call denite#custom#source('file_rec', 'matchers', ['matcher_fuzzy', 'matcher_ignore_globs'])

" == [ File Finder Configs ] ==========================================

" Let file_rec/git be an alias of file_rec (and use git ls-files).
call denite#custom#alias('source', 'file_rec/git', 'file_rec')
call denite#custom#var('file_rec/git', 'command', ['git', 'ls-files', '-co', '--exclude-standard'])

" Ag command on grep source
call denite#custom#var('grep', 'command', ['ag'])
call denite#custom#var('grep', 'default_opts', ['-i', '--vimgrep'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])

" Use "ag" (the silver searcher) for the file finder.
call denite#custom#var('file_rec', 'command', ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])

" == [ Define Custom Commands ] =======================================
call denite#custom#var('menu', 'menus', my_config#load_json('command_palette'))
