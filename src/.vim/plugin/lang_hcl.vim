" Terraform
let g:terraform_align = 1
let g:terraform_fold_sections = 1
let g:terraform_remap_spacebar = 1
let g:terraform_completion_keys = 1
let g:terraform_registry_module_completion = 0
autocmd FileType terraform setlocal commentstring=#%s
