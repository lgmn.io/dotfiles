" [ Vim Templates ] ====================================================
let g:license = "MIT"
let g:username = "Jake Logemann"
let g:email = "jake.logemann@gmail.com"
let g:templates_directory=[printf("%s/templates", my_config#get_path('vimdir'))]
let g:templates_no_builtin_templates = 1
let g:templates_fuzzy_start = 1
let g:templates_user_variables = [
      \   ['FULLPATH', 'GetFullPath'],
      \ ]

function! GetFullPath()
  return expand('%:p')
endfunction
