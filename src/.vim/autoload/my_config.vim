" if exists('g:loaded_my_config_lib') | finish | endif
" let g:loaded_my_config_lib = 1

let s:vimdir = expand('~/.vim')
let s:my_vimdir = expand('~/.vim')
if has("nvim") | let s:my_vimdir = expand('~/.config/nvim') | endif

" Runtime Files
" -------------
" Set Vim directories, double slashes indicate vim should avoid naming collisions by writing
" out the full path of the file.
let s:debug_mode = expand('$VIM_DEBUG') || 0
let s:my_local_dir = s:my_vimdir . '/local'
let s:my_snippets_dir = s:my_vimdir . '/snippet'
let s:my_spell_dir = s:my_vimdir . '/spell'
let s:my_plug_dir = s:my_local_dir . '/plugs'
let s:my_data_dir = s:vimdir . '/data'
let s:my_local_vimrc = s:my_local_dir . '/vimrc.local'
let s:my_local_vimrc_default = s:my_local_dir . '/vimrc.default.local'
let s:my_local_backups = s:my_local_dir . '/backup_files//'  "double slash intended here.
let s:my_local_swaps = s:my_local_dir . '/swap_files//'      "double slash intended here.
let s:my_local_undos = s:my_local_dir . '/undo_files//'      "double slash intended here.
let s:my_local_views = s:my_local_dir . '/views//'           "double slash intended here.
let s:my_local_viminfo = s:my_local_dir . '/viminfo'

function! my_config#preinit()
  "persistence features should be disabled if running as a root user to
  "prevent those pesky root-owned files from breaking things.
  if exists('$SUDO_USER')
    call my_config#unset_paths()
  else
    call my_config#touch_paths()
    call my_config#set_paths()
  endif

  "Warn the user if their viminfo is not readible.
  call my_config#ensure_readable(s:my_local_viminfo)
endfunction

function! my_config#get_path(var_name)
  " Return the named local variable, prefixed with 'my_', bound to this script.
  return get(s:, 'my_' . a:var_name, '')
endfunction

function! my_config#debug_mode()
  " Returns true if debug mode is enabled, else returns false.
  return get(s:, 'debug_mode', 0) != 0
endfunction

function! my_config#load_json(relative_path)
  " TODO: This needs error checking.
  let l:json_dir = my_config#get_path('data_dir')
  let l:json_file = fnameescape(l:json_dir . '/' . a:relative_path . '.json')
  let l:file_data = join(readfile(l:json_file), '')
  let l:json_data = json_decode(l:file_data)
  return l:json_data
endfunction

function! my_config#unset_paths()
  "Unsets all settings related to files persisted by Vim.
  set nobackup
  set noswapfile
  set noundofile
  set nowritebackup
  set viewdir=
  set viminfo=
endfunction

function! my_config#set_paths()
  "Resets all settings related to files persisted by Vim.
  execute printf('set %s=%s', 'backupdir', s:my_local_backups)
  execute printf('set %s=%s', 'directory', s:my_local_swaps)
  execute printf('set %s=%s', 'undodir', s:my_local_undos)
  execute printf('set %s=%s', 'viewdir', s:my_local_views)
  execute printf('set %s+=n%s', 'viminfo', s:my_local_viminfo)
  set undofile
endfunction

function! my_config#touch_paths()
  "Create directories used by my_config#set_path()
  echomsg 'creating local configuration directories.'
  call my_config#touch_dir(s:my_local_backups)
  call my_config#touch_dir(s:my_local_swaps)
  call my_config#touch_dir(s:my_local_undos)
  call my_config#touch_dir(s:my_local_views)
endfunction

function! my_config#ensure_readable(f)
  "warns the user if there is a given file but it is not readable.
  if !empty(glob(a:f))
    if !filereadable(a:f)
      echoerr 'warning: ' . a:f . ' exists but is not readable'
    endif
  endif
endfunction

function! my_config#touch_dir(d)
  "Create a given directory if it doesn't already exist.
  echomsg printf('silent !mkdir -p %s || true', a:d)
  execute printf('silent !mkdir -p %s || true', a:d)
endfunction

function! my_config#source(relpath)
  execute printf('source %s/%s', s:vimdir, a:relpath)
endfunction

function! my_config#source_local_vimrc()
  " Ensure local vimrc exists (copy the default if needed)
  if empty(glob(s:my_local_vimrc))
    execute printf('silent !cp %s %s', s:my_local_vimrc_default, s:my_local_vimrc)
  endif

  " Source the local vimrc file.
  execute printf('source %s', s:my_local_vimrc)
endfunction

function! my_config#install_plug()
  let plug_url = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  let plug_dst = printf('%s/autoload/%s', s:my_vimdir, 'plug.vim')

  " If Plug is already installed, bail.
  if !empty(glob(plug_dst)) | return 0 | endif

  echo "Downloading 'junegunn/vim-plug' ..."
  execute printf("silent !curl -sfL --create-dirs -o %s %s", plug_dst, plug_url)

  echo "Loading 'junegunn/vim-plug' ..."
  call my_config#source('autoload/plug.vim')
endfunction

function! my_config#plugins_begin()
  call my_config#install_plug()
  call plug#begin(s:my_plug_dir)
endfunction

function! my_config#plugins_end()
  call plug#end()
endfunction

function! my_config#is_modern_vim()
  return has('nvim') || ! v:version < 800
endfunction

