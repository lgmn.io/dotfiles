function! my_fn#tab_edit_bookmarks()
  execute 'tabedit' . g:NERDTreeBookmarksFile
endfunction

function! my_fn#stahpit(msg)
  echom '[STAHP IT RITE NOW.] :: ' . a:msg
endfunction

function! my_fn#fix_whitespace()
  let l:old_pos = winsaveview()
  %s/\s\+$//e
  retab
  call winrestview(l:old_pos)
endfunction
