
*my_help.txt*                                         by Jake Logemann

                              Jake's VIM


Using My Vim                                           *my-vim*

This is a help document to tuck all of the random things I constantly
need to lookup in Vim.

1. Autocompletion                                   |my-autocompletion|
2. Autocompletion                                   |my-autocompletion|
I. Useful help documents.                           |my-useful-docs|

==============================================================================

1. Editing / Creating Files                                 *my-editor*

Use Netrw More:
  vim scp://foohost/.ssh/config   # Edit the SSH Config on a remote host.
  vim scp://foohost/.ssh          # Browse the SSH Dir on a remote host.

==============================================================================

2. Autocompletion                                   *my-autocompletion*

In insert mode:
  '^x^o'   "Omni-complete"
  '^x^f'   Insert a file path.
  '^x^l'   Autocomplete another line.

==============================================================================

I. Useful help documents.                              *my-useful-docs*

  All Plugin Documentation: ':help local-additions' VIM Reference
  Documentation: ':help reference_toc' Search VIM Documentation:
  ':helpgrep' (^N/^P to navigate)

=========================================================================
vim:tw=78:ts=8:ft=help:norl:
