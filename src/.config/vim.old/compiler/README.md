Files in `~/.vim/compiler/` work exactly like indent files. They should set compiler-related options in the current buffer based on their names.
