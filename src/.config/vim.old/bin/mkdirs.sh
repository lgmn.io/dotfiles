#! /bin/sh
#
# mkdirs.sh
# Copyright (C) 2017 Jake Logemann <jake.logemann@gmail.com>
#
# Distributed under terms of the MIT license.
#


mkdir ~/.vim/local/{backup_files,swap_files,views,undo_files}
