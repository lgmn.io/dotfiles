The `~/.vim/autoload/` directory is an incredibly important hack. It sounds a lot more complicated than it actually is.

In a nutshell: `autoload` is a way to delay the loading of your plugin's code until it's actually needed.
