if exists('g:loaded_my_autocomplete_lib') | finish | endif
let g:loaded_my_autocomplete_lib = 1

" By default, theres no expansion open.
let s:expansion_active = 0

" Generate UltiSnips keybinds from the YouCompleteMe ones.
let s:UltiSnipsExpandTrigger = g:ycm_key_list_accept_completion[0]
let s:UltiSnipsJumpForwardTrigger = g:ycm_key_list_select_completion[0]
let s:UltiSnipsJumpBackwardTrigger = g:ycm_key_list_previous_completion[0]
let s:UltiSnipsListSnippets = '<C-e>'

function! my_autocomplete#inject()  " {{{
  let g:UltiSnipsExpandTrigger = s:UltiSnipsExpandTrigger
  let g:UltiSnipsJumpForwardTrigger = s:UltiSnipsJumpForwardTrigger
  let g:UltiSnipsJumpBackwardTrigger = s:UltiSnipsJumpBackwardTrigger
  let s:UltiSnipsListSnippets = s:UltiSnipsListSnippets

  " Ensure the snippets directory is properly set.
  let g:UltiSnipsSnippetsDir = my_config#get_path('snippets_dir')

  " Prevent UltiSnips from removing our carefully-crafted mappings.
  let g:UltiSnipsMappingsToIgnore = ['autocomplete']

  if has('autocmd')
    augroup MyAutocomplete
      autocmd!
      autocmd! User UltiSnipsEnterFirstSnippet
      autocmd  User UltiSnipsEnterFirstSnippet call my_autocomplete#set_mappings()
      autocmd! User UltiSnipsExitLastSnippet
      autocmd  User UltiSnipsExitLastSnippet   call my_autocomplete#unset_mappings()
    augroup END
  endif
endfunction  " }}}

function! my_autocomplete#set_mappings()  " {{{
  " Go Forwards.
  call s:snippet_smap(g:UltiSnipsJumpForwardTrigger,   'call my_autocomplete#complete_forward()')
  call s:snippet_imap(g:UltiSnipsJumpForwardTrigger,   'my_autocomplete#complete_forward()')

  " Go Backwards.
  call s:snippet_smap(g:UltiSnipsJumpBackwardTrigger,  'call my_autocomplete#complete_backward()')
  call s:snippet_imap(g:UltiSnipsJumpBackwardTrigger,  'my_autocomplete#complete_backward()')

  " Accept an autocompletion or insert a verbatim <CR>
  imap <expr> <buffer> <silent> <C-l> my_autocomplete#complete_accept()
  smap <expr> <buffer> <silent> <C-l> call my_autocomplete#complete_accept()

  let s:expansion_active = 1
endfunction  " }}}

function! my_autocomplete#unset_mappings()  " {{{
  silent! iunmap        <buffer> g:UltiSnipsJumpBackwardTrigger
  silent! iunmap        <buffer> g:UltiSnipsJumpForwardTrigger
  silent! iunmap <expr> <buffer> g:UltiSnipsExpandTrigger
  silent! sunmap        <buffer> g:UltiSnipsJumpBackwardTrigger
  silent! sunmap        <buffer> g:UltiSnipsJumpForwardTrigger
  silent! sunmap <expr> <buffer> g:UltiSnipsExpandTrigger

  let s:expansion_active = 0
endfunction  " }}}

function! my_autocomplete#complete_forward()  " {{{
  " If UltiSnips had a result, it did something.
  call UltiSnips#JumpForwards()
  if get(g:, 'ulti_jump_forwards_res', 0) != 0 | return "" | endif

  " If the popup window is open, select next item.
  if pumvisible() | return "\<C-j>" | endif

  return printf("\\%s", g:UltiSnipsJumpForwardTrigger)
endfunction  " }}}

function! my_autocomplete#complete_backward()  " {{{
  " If UltiSnips had a result, it did something.
  call UltiSnips#JumpBackwards()
  if get(g:, 'ulti_jump_backwards_res', 0) != 0 | return "" | endif

  " If the popup window is open, select prev item.
  if pumvisible() | return "\<C-k>" | endif

  return printf("\\%s", g:UltiSnipsJumpBackwardTrigger)
endfunction  " }}}

function! my_autocomplete#complete_accept()  " {{{
  " If UltiSnips had a result, it did something.
  call UltiSnips#ExpandSnippet()
  if get(g:, 'ulti_expand_res', 0) != 0 | return "" | endif

  " If the popup window is open, select that item.
  if pumvisible() | return "\<C-l>" | endif

  return ""
endfunction  " }}}

function! s:snippet_smap(bind, fn)  " {{{
  execute printf('snoremap <buffer> <silent> %s <Esc>:%s<cr>', a:bind, a:fn)
endfunction  " }}}

function! s:snippet_imap(bind, fn)  " {{{
  execute printf('inoremap <buffer> <silent> %s <C-R>=%s<cr>', a:bind, a:fn)
endfunction  " }}}

" vim: foldenable foldlevel=0 foldmethod=marker
