if exists('g:loaded_my_autogroup_lib') | finish | endif
let g:loaded_my_autogroup_lib = 1

function my_autogroups#inject()  "{{{
  call my_autogroups#inject_vim_startup()
  call my_autogroups#inject_auto_relativenum()
  call my_autogroups#inject_auto_fix()
endfunction  " }}}

function my_autogroups#inject_vim_startup()  "{{{
  " Vim Startup Commands
  augroup my_vim_startup
    autocmd!

    "Automatically start Netrw if no file given.
    autocmd VimEnter * if expand("%") == "" | edit . | endif

    "Always expect files to be UTF-8 (nvim can only set this once).
    if has("multi_byte")
      " For all windows, assume file encoding is UTF-8.
      set fileencoding=utf-8
      set fileencodings=utf-8,latin1

      " Ensure termencoding is defined or just use &fileencoding.
      if &termencoding == "" | let &termencoding = &fileencoding | endif

      "Automatically reload vimrc(s) on save.
      autocmd BufWritePost $MYVIMRC nested     source %
      autocmd BufWritePost ~/.vim/init.vim     source %
      autocmd BufWritePost ~/.vim/my_base.vim  source %

      " Any time anything in local docs is updated, rebuild vim's tags for it.
      autocmd BufWritePost ~/.vim/doc/* helptags ~/.vim/doc
    endif
  augroup END
endfunction  " }}}

function my_autogroups#inject_auto_relativenum() "{{{
  " Automatically toggle relative line numbers on/off based on INSERT or
  " NORMAL mode.
  augroup toggle_relativenum
    autocmd!
    autocmd InsertLeave * setlocal nonumber relativenumber
    autocmd InsertEnter * setlocal number norelativenumber
  augroup END
endfunction  " }}}

function my_autogroups#inject_auto_fix() "{{{
  augroup autofix_files
    autocmd!

    " Automatically fix tabs in files before saving.
    autocmd BufWritePre * retab

    " Automatically trim whitespace before saving.
    autocmd BufWritePre * %s/\s\+$//e

    " Automatically rewrap the following filetypes
    " autocmd BufWritePre *.txt normal ggVGgq``
    " autocmd BufWritePre *.md  normal ggVGgq``
  augroup END
endfunction  " }}}
