Files in `~/.vim/indent/` are a lot like `ftplugin` files. They get loaded based on
their names.

indent files should set options related to indentation for their `filetypes`, and
those options should be buffer-local.

Yes, you could simply put this code in the `ftplugin` files, but it's better to
separate it out so other Vim users will understand what you're doing. It's just
a convention, but please be a considerate plugin author and follow it.
