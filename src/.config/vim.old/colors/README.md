Files inside `~/.vim/colors/` are treated as color schemes. For example: if you
run `:color mycolors` Vim will look for a file at `~/.vim/colors/mycolors.vim` and
run it. That file should contain all the `Vimscript` commands necessary to
generate your color scheme.

