"..........................................................."
".   .d8888b.           888                                ."
".  d88P  Y88b          888                                ."
".  888    888          888                                ."
".  888         .d88b.  888  8888b.  88888b.   .d88b.      ."
".  888  88888 d88""88b 888     "88b 888 "88b d88P"88b     ."
".  888    888 888  888 888 .d888888 888  888 888  888     ."
".  Y88b  d88P Y88..88P 888 888  888 888  888 Y88b 888     ."
".   "Y8888P88  "Y88P"  888 "Y888888 888  888  "Y88888     ."
".   --------------------------------------------- 888     ."
".                                            Y8b d88P     ."
".                                             "Y88P"      ."
"..........................................................."
let g:go_def_reuse_buffer = 1
let g:go_fmt_autosave = 1
let g:go_fmt_command = "goimports"
let g:go_fmt_experimental = 1
let g:go_fmt_fail_silently = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 0
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_types = 1
let g:go_list_height = 5
let g:go_metalinter_autosave = 1
let g:go_metalinter_autosave_enabled = ['vet', 'golint']
let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
let g:go_snippet_engine = "neosnippet"
let g:go_template_autocreate = 0


"..........................................................."
".  8888888b.           888    888                         ."
".  888   Y88b          888    888                         ."
".  888    888          888    888                         ."
".  888   d88P 888  888 888888 88888b.   .d88b.  88888b.   ."
".  8888888P"  888  888 888    888 "88b d88""88b 888 "88b  ."
".  888        888  888 888    888  888 888  888 888  888  ."
".  888        Y88b 888 Y88b.  888  888 Y88..88P 888  888  ."
".  888         "Y88888  "Y888 888  888  "Y88P"  888  888  ."
".   -------------- 888 --------------------------------   ."
".             Y8b d88P                                    ."
".              "Y88P"                                     ."
"..........................................................."
"
" TODO: Add python-specific configurations.



"..........................................................."
".                                                         ."
".  8888888b.           888                                ."
".  888   Y88b          888                                ."
".  888    888          888                                ."
".  888   d88P 888  888 88888b.  888  888                  ."
".  8888888P"  888  888 888 "88b 888  888                  ."
".  888 T88b   888  888 888  888 888  888                  ."
".  888  T88b  Y88b 888 888 d88P Y88b 888                  ."
".  888   T88b  "Y88888 88888P"   "Y88888                  ."
".   -------------------------------- 888                  ."
".                               Y8b d88P                  ."
".                                "Y88P"                   ."
"..........................................................."
let g:rubycomplete_buffer_loading = 1
"let g:rubycomplete_classes_in_global = 1
"let g:rubycomplete_rails = 1


"..........................................................."
".  8888888b.                    888                       ."
".  888   Y88b                   888                       ."
".  888    888                   888                       ."
".  888   d88P 888  888 .d8888b  888888                    ."
".  8888888P"  888  888 88K      888                       ."
".  888 T88b   888  888 "Y8888b. 888                       ."
".  888  T88b  Y88b 888      X88 Y88b.                     ."
".  888   T88b  "Y88888  88888P'  "Y888                    ."
"..........................................................."
"
" TODO: Add rust-specific configurations.
