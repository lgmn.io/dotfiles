function! KubectlTypes(ArgLead, CmdLine, CursorPos)
  let t = system("sh -c \"kubectl get --help 2>&1 | grep -E '^\W+\*' | awk '{print $2}'\"")
  echom l:t
  return l:t
endfunction

command! -nargs=0 -complete=file  KubectlTypes     call KubectlTypes('','', 0)
command! -nargs=* -complete=file  Kubectl          !kubectl <args><CR>
command! -nargs=* -complete=file  KubectlApply     !kubectl apply -f <f-args><CR>
command! -nargs=* -complete=file  KubectlDescribe  !kubectl describe -f <f-args><CR>
command! -nargs=* -complete=file  KubectlView      !kubectl get -o yaml -f <f-args><CR>
command! -nargs=* -complete=file  KubectlDelete    !kubectl delete --cascade -f <f-args><CR>
command! -nargs=* -complete=file  KubectlDelete    !kubectl delete --cascade -f <f-args><CR>

cabbrev k8s          Kubectl
cabbrev k8sApply     KubectlApply %
cabbrev k8sDelete    KubectlDelete %
cabbrev k8sDescribe  KubectlDescribe %
