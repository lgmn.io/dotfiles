" TagBar

let g:tagbar_autoclose = 1

" NERDTree
let g:NERDTreeAutoCenter          = 1
let g:NERDTreeAutoDeleteBuffer    = 1
let g:NERDTreeBookmarksSort       = 1
let g:NERDTreeCascadeSingleChild  = 0
let g:NERDTreeChDirMode           = 2
let g:NERDTreeHighlightCursorline = 0
let g:NERDTreeHijackNetrw         = 1
let g:NERDTreeMinimalUI           = 0
let g:NERDTreeQuitOnOpen          = 1
let g:NERDTreeRespectWildIgnore   = 1
let g:NERDTreeShowBookmarks       = 1
let g:NERDTreeShowHidden          = 1
let g:NERDTreeShowLineNumbers     = 0
let g:NERDTreeDirArrowCollapsible = '-'
let g:NERDTreeDirArrowExpandable  = '+'
let g:NERDTreeCreatePrefix        = 'silent keepalt keepjumps'

" Netrw (File browser) Configs
" ----------------------------
let g:netrw_altv = 0                              "Open splits to the right.
let g:netrw_keepdir = 0                           "Keep track of remote directory.
let g:netrw_banner = 0                            "Disable annoying banner.
let g:netrw_browse_split = 0                      "Use current window.
let g:netrw_list_hide  = netrw_gitignore#Hide()   "Ignore files in .gitignore.
" let g:netrw_list_hide .= ',\(^\|\s\s\)\zs\.\S\+'  "Hide
let g:netrw_liststyle = 3                       "Use TREE view instead of list.

