" Project-Specific Configurations
" -------------------------------
let g:projectionist_heuristics = { }
let g:projectionist_heuristics["GoProject"] = {
      \     "*/*.go": {
      \       "type": "src",
      \       "dispatch": "golint"
      \     },
      \   }

let g:projectionist_heuristics["ChefCookbook"] = {
      \     "Berksfile|Gemfile|Gemfile.lock": {
      \       "type": "reqs",
      \       "dispatch": "make clean"
      \     },
      \     "recipes/*.rb": {
      \       "type": "recipe",
      \       "dispatch": "make style"
      \     },
      \     "attributes/*.rb": {
      \       "type": "attr",
      \       "dispatch": "make style"
      \     },
      \     "templates/*": {
      \       "type": "tmpl",
      \       "dispatch": "make kitchen"
      \     },
      \ }

