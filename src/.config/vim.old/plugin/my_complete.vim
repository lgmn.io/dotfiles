" [ NeoComplete ] =====================================================
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length = 3

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Plugin key-mappings.
inoremap <expr><BS>     neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-g>    neocomplete#undo_completion()
inoremap <expr><C-h>    neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-j>    pumvisible() ? "\<C-n>" : "\<C-j>"
inoremap <expr><C-k>    pumvisible() ? "\<C-p>" : "\<C-k>"
inoremap <expr><C-l>    neocomplete#complete_common_string()
inoremap <expr><Space>  pumvisible() ? "\<C-y><Space>" : "\<Space>"
inoremap <expr><TAB>    pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB>  pumvisible() ? "\<C-p>" : "\<S-TAB>"

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
endfunction

" [ NeoSnippet ] =======================================================
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>          <Plug>(neosnippet_expand_or_jump)
smap <C-k>          <Plug>(neosnippet_expand_or_jump)
xmap <C-k>          <Plug>(neosnippet_expand_target)
smap <expr><TAB>    neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" [ Vim Templates ] ====================================================
let g:license = "MIT"
let g:username = "Jake Logemann"
let g:email = "jake.logemann@gmail.com"
let g:templates_directory=[printf("%s/templates", my_config#get_path('vimdir'))]
let g:templates_no_builtin_templates = 1
let g:templates_fuzzy_start = 1
let g:templates_user_variables = [
      \   ['FULLPATH', 'GetFullPath'],
      \ ]

function! GetFullPath()
  return expand('%:p')
endfunction
