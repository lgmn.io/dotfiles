if has('nvim')
  set termguicolors
endif

set background=dark
colorscheme Tomorrow-Night-Eighties
