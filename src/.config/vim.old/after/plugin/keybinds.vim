" Quitting Time.
nnoremap <silent> <C-c>             :close<cr>
nnoremap <silent> <C-c><C-c><C-c>   :qa!<cr>

" Quickly clear search highlighting.
nnoremap <silent> <bs><bs>          :nohlsearch<cr>

" Retraining my fingers...
" nnoremap <silent> <C-e> :call my_fn#stahpit('Use "-" to get to the file browser.')<CR>


" Buffer/Tab Navigation
" ---------------------

" GoTo Buffer.
nnoremap <silent> gb       :bnext<cr>
nnoremap <silent> gB       :bprevious<cr>

" GoTo Tab.
nnoremap <silent> gt       :tabnext<cr>
nnoremap <silent> gT       :tabprevious<cr>

" Arrow Keys Navigate Buffers & Tabs.
nnoremap <silent> <Up>     :bnext<cr>
nnoremap <silent> <Down>   :bprevious<cr>
nnoremap <silent> <Right>  :tabnext<cr>
nnoremap <silent> <Left>   :tabprevious<cr>

" Chords
" ------
nnoremap <silent> <C-e>  :NERDTreeToggle<cr>

" Function Keys
" -------------
nnoremap <silent> <F1>  :tabdo help<cr>
nnoremap <silent> <F2>  :exec 'tabedit' . expand($MYVIMRC) <cr>
nnoremap <silent> <F3>  :call my_fn#tab_edit_bookmarks()<CR>


" Leader Mappings
" ---------------
let g:mapleader = ' '   "Space is the best leader key. Srsly.

" Disable the leader key's normal/visual function.
nnoremap <silent> <leader> <Nop>
vnoremap <silent> <leader> <Nop>

nnoremap <silent> <leader><space>   :Denite menu:root<cr>

" [C]ompletions
nnoremap <silent> <leader>c   <Nop>
nnoremap <silent> <leader>cc  :NeoCompleteToggle<cr>
nnoremap <silent> <leader>cx  :NeoCompleteClean<cr>

nnoremap <silent> <leader>cd  :YcmShowDetailedDiagnostic<cr>

" [G]it Commands
nnoremap <silent> <leader>gs    :GStatus<cr>
nnoremap <silent> <leader>gw    :GWrite<cr>
nnoremap <silent> <leader>gci   :GCommit<cr>
nnoremap <silent> <leader>gb    :GBlame<cr>
nnoremap <silent> <leader>gpl   :Git pull<cr>
nnoremap <silent> <leader>gps   :Git push<cr>

" [O]pening Menus.
nnoremap <silent> <leader>o   <Nop>
nnoremap <silent> <leader>og  :call quickmenu#toggle(2)<cr>
nnoremap <silent> <leader>ow  :call quickmenu#toggle(3)<cr>
nnoremap <silent> <leader>oi  :call quickmenu#toggle(4)<cr>
nnoremap <silent> <leader>oq  :call quickmenu#toggle(98)<cr>
nnoremap <silent> <leader>ov  :call quickmenu#toggle(99)<cr>
nnoremap <silent> <leader>ot  :TagbarToggle<cr>

" [S]nippets.
nnoremap <silent> <leader>s   <Nop>
nnoremap <silent> <leader>se :NeoSnippetEdit<cr>

" [T]abularize / [T]ext Helpers.
nnoremap <silent> <leader>t    <Nop>
nnoremap <silent> <leader>tt   :call my_fn#fix_whitespace()<cr>
vnoremap <silent> <leader>ts   :Tabularize multiple_spaces<cr>

if !has('nvim') | finish | endif

" NeoVim Terminal
tnoremap <C-Esc><C-Esc> <C-\><C-n>
tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

