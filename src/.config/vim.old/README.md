# My (Neo)vim configs

## Install
```sh
git clone --recursive git@gitlab.com:JakeLogemann/vim.git $HOME/.vim && $HOME/.vim

./bin/unpack
```

## Install NeoVim
```sh
# Install python3 & deps.
sudo apt-get install -qy python-dev python-pip python3-dev python3-pip software-properties-common

# Add NeoVim PPA.
sudo add-apt-repository "ppa:neovim-ppa/unstable" && sudo apt-get update

# Install NeoVim & python extension.
sudo apt-get install neovim
sudo pip3 install neovim

# Update alternatives
# sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
# sudo update-alternatives --config vi
# sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
# sudo update-alternatives --config vim
# sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
[#](#) sudo update-alternatives --config editor
```
