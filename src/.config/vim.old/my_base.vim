filetype plugin on                               "Enable plugins in files.
syntax on

" General VIM Options
set autowrite                                    "Automatically write all buffers on :make or similar.
set background=dark                              "Use dark colorscheme.
set hidden                                       "Allows hiding buffers w/out saving first.
set laststatus=2                                 "always show status line
set lazyredraw                                   "don't bother updating screen during macro playback
set nosplitbelow splitright                      "Buffers are always on top-right, not top-left.
set number                                       "Show line numbers.
set path+=**                                     "Automatically search subdirectories for things like <esc>gf
set timeoutlen=500 ttimeoutlen=100               "Lower keypress-delay.
set wildmenu                                     "Enable 'wildmenu' autocompletion.

" Default Text Options.
set expandtab                                    "Expand tabs to spaces unless told otherwise.
set hlsearch                                     "Highlight search terms in the current buffer.
set nospell                                      "Disable spellcheck.
set nowrap                                       "Disable (by default) text wrapping.
set shiftwidth=2                                 "Set tabwidth.
set softtabstop=2                                "Set tabwidth.
set tabstop=2                                    "Set tabwidth.
set textwidth=0                                  "By default, no text-wrapping should occur.

" Highlighting Options.
set highlight+=@:ColorColumn                     "~/@ at end of window, 'showbreak'
set highlight+=N:DiffText                        "make current line number stand out a little
set highlight+=c:LineNr                          "blend vertical separators with line numbers

" 'Short Message'
" ===============
"   Flags Used:
"     A ......... ignore annoying swapfile messages
"     I ......... no splash screen
"     O ......... file-read message overwrites previous
"     T ......... truncate non-file messages in middle
"     W ......... don't echo "[w]"/"[written]" when writing
"     a ......... use abbreviations in messages eg. `[RO]` instead of `[readonly]`
"     o ......... overwrite file-written messages
"     t ......... truncate file messages at start
set shortmess+=AIOTWaot

if has('mouse')
  set mouse=a
  set mousehide                                    "Turn off mouse when typing
endif

if has('folding')
  set foldlevel=0                                  "Fold everything.
  set foldmethod=syntax                            "Fold by syntax instead of 'manual'.
  set nofoldenable                                 "Enable folding by default.
endif

if has('clipboard')
  set clipboard=unnamedplus                        "Use default Linux X11 Clipboard.
endif

if has('conceal')
  set conceallevel=0                               "Conceal nothing.
endif

if has('listcmds')
  set nolist                                       "Hide list characters by default.
  set listchars=tab:>\ ,trail:-,nbsp:+             "Define hidden characters to show.
endif

if has('linebreak')
  let &showbreak='⤷ '                 " ARROW POINTING DOWNWARDS THEN CURVING RIGHTWARDS (U+2937, UTF-8: E2 A4 B7)
endif

if has('syntax')
  set spellcapcheck=                  " don't check for capital letters at start of sentence
endif

if has('virtualedit')
  set virtualedit=block               " allow cursor to move where there is no text in visual block mode
endif

if has('gui_running')
  " set guicursor=a:blinkon0   " turn off cursor blinking
  set guicursor=a:blinkon600-blinkoff400  " Slow down cursor blinking speed
  set guifont=Inconsolata\ 14
  set guioptions+=c            " Use console dialogs when possible
  set guioptions-=L            " Remove left-hand scrollbar (for v-split)
  set guioptions-=T            " Remove toolbar
  set guioptions-=m            " remove menu bar
  set guioptions-=r            " Remove right hand scrollbar
  set t_Co=                    " Terminal colors are not used in the GUI and should be removed.
  set visualbell               " Enable visualbell (this is the WM's urgent label).
  set t_vb=                    " Turn off flashing (needs to be in gvimrc too)
elseif &t_Co > 2   " If the terminal has 2 or more colors...
  set t_Co=256     "   Assume there's 256 color support.
endif

call my_autogroups#inject()
