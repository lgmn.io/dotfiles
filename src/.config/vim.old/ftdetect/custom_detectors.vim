function! s:set_yaml_filetype()
  let hasApiVersion = match(readfile(expand("%:p")), "apiVersion:") != -1
  if hasApiVersion
    setfiletype yaml.k8s
  else
    setfiletype yaml
  endif
endfunction

augroup custom_file_detectors
  autocmd!
  autocmd  BufNewFile,BufRead *.log         setl filetype=log
  autocmd  BufNewFile,BufRead *.sls         setl filetype=yaml.salt
  autocmd  BufNewFile,BufRead *.Dockerfile  setl filetype=dockerfile
  autocmd  BufNewFile,BufRead Dockerfile.*  setl filetype=dockerfile
  autocmd  BufNewFile,BufRead *.envrc*      setl filetype=sh
  autocmd  BufNewFile,BufRead *.tfstate     setl filetype=json
  autocmd  BufNewFile,BufRead *.tfstate*    setl filetype=json
  autocmd  BufNewFile,BufRead zshrc.local   setl filetype=zsh
  autocmd  BufNewFile,BufRead vimrc.local   setl filetype=vim
  autocmd  BufNewFile,BufRead *.yaml,*.yml  call s:set_yaml_filetype()
  autocmd  BufNewFile,BufRead config        call s:set_yaml_filetype()
augroup END
