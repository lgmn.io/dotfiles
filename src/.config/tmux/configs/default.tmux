source-file ~/.config/tmux/lib/base.tmux
source-file ~/.config/tmux/lib/keybinds.tmux
# source-file ~/.config/tmux/lib/plugins.tmux
source-file ~/.config/tmux/lib/theme.tmux

# vi: ft=tmux
