# TMUX Plugins (tpm) 
run '~/.config/tmux/scripts/setup.sh'

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @plugin 'tmux-plugins/tmux-logging'
# set -g @plugin 'git@github.com/tmux-plugins/tmux-open'

# Finally, Initialize TMUX plugin manager.
run '~/.config/tmux/plugins/tpm/tpm'

# vi: ft=tmux
