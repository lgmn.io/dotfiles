# General Options 
set -g   base-index         1
set -g   default-terminal   "screen-256color"
set -g   default-terminal   "screen-256color"
set -g   set-titles         on
set -g   set-titles-string  '#T - #I:#W'
setw -g  monitor-activity   on
setw -g  pane-base-index    1

# address vim mode switching delay (http://superuser.com/a/252717/65504)
set -s escape-time 0

# increase scrollback buffer size
set -g history-limit 50000

# tmux messages are displayed for 4 seconds
set -g display-time 3000

# upgrade $TERM
set -g default-terminal "screen-256color"

# focus events enabled for terminals that support them
set -g focus-events on

# super useful when using "grouped sessions" and multi-monitor setup
setw -g aggressive-resize on

setw -g automatic-rename off

# Disable Bells/Notifications
set -g   visual-activity   off
set -g   visual-bell       off
set -g   visual-silence    off
set -g   bell-action       none

# Mouse support
# set -g   mouse-select-pane    on
# set -g   mouse-resize-pane    on
# set -g   mouse-select-window  on
# setw -g  mode-mouse           copy-mode

# vi: ft=tmux
