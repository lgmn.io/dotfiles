# Theme Options {{{1

## Panes {{{2
set -g  pane-active-border-bg  black
set -g  pane-active-border-fg  brightred
set -g  pane-border-bg         black
set -g  pane-border-fg         brightblack

## Status Bar {{{2
#set -g status-utf8 on
set -g status-attr dim
set -g status-bg black
set -g status-fg white
set -g status-interval 1
set -g status-justify left
set -g status-left ' @#S '
set -g status-left-length 16
set -g status-position top
set -g status-right '#[bg=colour0,bold] #[fg=colour2]%d/%m #[fg=colour4] %H:%M:%S '
set -g status-right-length 20

# emacs key bindings in tmux command prompt (prefix + :) are better than
# vi keys, even for vim users
set -g status-keys emacs

## Messaging {{{2
set -g  message-attr                   bold
set -g  message-bg                     black
set -g  message-fg                     black

set -g  message-command-attr           bold
set -g  message-command-bg             black
set -g  message-command-fg             blue

## Windows {{{2
setw -g  window-status-bell-attr       bold,dim
setw -g  window-status-bell-bg         black
setw -g  window-status-bell-fg         red

setw -g  window-status-attr            dim
setw -g  window-status-bg              black
setw -g  window-status-fg              white
setw -g  window-status-format          ' #I:#[fg=default]#W#[fg=blue,none]#F#[default] '
# setw -g  window-status-seperator       '|'

setw -g  window-status-current-attr    bold,bright
setw -g  window-status-current-bg      black
setw -g  window-status-current-fg      yellow
setw -g  window-status-current-format  '[#I:#[fg=blue,underscore]#W#[fg=colour50,none]#F#[default]]'

## Modes {{{2
setw -g  clock-mode-colour  colour135

setw -g  mode-keys  vi
setw -g  mode-attr  bold
setw -g  mode-bg    black
setw -g  mode-fg    brightred


# vi: ft=tmux
