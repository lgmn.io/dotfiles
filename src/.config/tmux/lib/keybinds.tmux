# Key bindings {{{1
# Rebind global prefix to <Ctrl><Space> {{{2
unbind C-b
set -g prefix C-Space
bind Space send-prefix

# Custom Keybindings {{{2
bind C-d      detach
bind C-r      source-file ~/.tmux.conf \; display-message "Configuration reloaded."
bind C-Space  last-window
bind C-c      command-prompt -p command: "new-window -n '%1' '%1'"

# vi: ft=tmux
