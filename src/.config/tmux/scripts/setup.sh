#!/bin/bash
set -euo posix

readonly TMUX_DIR="$HOME/.tmux"
readonly TMUX_PLUGINS_DIR="$TMUX_DIR/plugins"

try_to(){ eval "$@ 2>/dev/null || true" ;}
touch_dir(){ try_to mkdir -p $@ ;}
bin_installed(){ try_to which --skip-functions --skip-aliases $@ ;}

touch_dir "$TMUX_PLUGINS_DIR"

if [[ ! -d "$TMUX_PLUGINS_DIR/tpm" ]]; then
  echo "# Tmux Plugin Manager is not installed... resolving that..."
  git clone "https://github.com/tmux-plugins/tpm" "$TMUX_PLUGINS_DIR/tpm"
fi

if [[ ! -x $(bin_installed "tmuxp") ]]; then
  echo "# TmuxP is not installed... resolving that..."
  pip install --user tmuxp
fi
