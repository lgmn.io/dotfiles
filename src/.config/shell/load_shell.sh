
list_source_files(){
  _shell_name=`basename $SHELL`

  # If the directory doesn't exist, dont bother listing source files.
  test -d "$1" || return

  # Load all shell scripts in profile directory.
  find "$1/" -type f \
    -iname "*.sh" -o -iname "*.$_shell_name" \
    | sort -u

  # Load all local shell scripts in profile directory.
  find "$1/" -type f \
    \( -iname "*.sh" -o -iname "*.$_shell_name" \) \
    -iname "*.local.*" | sort -u
}

for f in `list_source_files "$HOME/.config/shell/env"`; do . "$f"; done
for f in `list_source_files "$HOME/.config/shell/aliases"`; do . "$f"; done

unset list_source_files
