export FZF_ROOT="$HOME/.fzf"
export FZF_DEFAULT_COMMAND="fzf"
export FZF_DEFAULT_OPTS='--color=dark --margin=1,2 --reverse --inline-info'

test -e "$FZF_ROOT" || {
  git clone "https://github.com/junegunn/fzf.git" "$FZF_ROOT"

  # Install the binary, but not the custom configs.
  $FZF_ROOT/install --bin
}

# Setup fzf
# ---------
PATH_append "$FZF_ROOT/bin"

# Auto-completion
# ---------------
if [[ $- == *i* ]]; then
  source "$FZF_ROOT/shell/completion.$(basename $SHELL)" 2> /dev/null
  source "$FZF_ROOT/shell/key-bindings.$(basename $SHELL)"
fi

# Fuzzy Finder (FZF) Aliases 
alias fzf-env='env | sort -u | fzf -1 | cut -d "=" -f 2- | xsel -ib'
