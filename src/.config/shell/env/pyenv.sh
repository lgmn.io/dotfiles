export PYENV_ROOT="$HOME/.pyenv"

test -e "$PYENV_ROOT" || git clone \
  "https://github.com/pyenv/pyenv.git" \
  "$PYENV_ROOT"

# Ensure "pyenv-virtualenv" plugin is installed.
test -e "$PYENV_ROOT/plugins/pyenv-virtualenv" || git clone \
  "https://github.com/pyenv/pyenv-virtualenv.git" \
  "$PYENV_ROOT/plugins/pyenv-virtualenv"

# Ensure "pyenv-doctor" plugin is installed.
test -e "$PYENV_ROOT/plugins/pyenv-doctor" || git clone \
  "https://github.com/pyenv/pyenv-doctor.git" \
  "$PYENV_ROOT/plugins/pyenv-doctor"

# Ensure "pyenv-update" plugin is installed.
test -e "$PYENV_ROOT/plugins/pyenv-update" || git clone \
  "https://github.com/pyenv/pyenv-update.git" \
  "$PYENV_ROOT/plugins/pyenv-update"

# Ensure "pyenv-which-ext" plugin is installed.
test -e "$PYENV_ROOT/plugins/pyenv-which-ext" || git clone \
  "https://github.com/pyenv/pyenv-which-ext.git" \
  "$PYENV_ROOT/plugins/pyenv-which-ext"

# Load PyEnv into the environment.
PATH_append "$PYENV_ROOT/bin"

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
