export RBENV_ROOT="$HOME/.rbenv"
# export RUBY_CONFIGURE_OPTS="--enable-shared"

test -e "$RBENV_ROOT" || git clone \
  "https://github.com/rbenv/rbenv.git" \
  "$RBENV_ROOT"

# Ensure "ruby-build" plugin is installed (for building ruby versions).
test -e "$RBENV_ROOT/plugins/ruby-build" || git clone \
  "https://github.com/rbenv/ruby-build.git" \
  "$RBENV_ROOT/plugins/ruby-build"

# Ensure "rbenv-default-gems" plugin is installed (for pre-installed gems).
test -e "$RBENV_ROOT/plugins/rbenv-default-gems" || git clone \
  "https://github.com/rbenv/rbenv-default-gems.git" \
  "$RBENV_ROOT/plugins/rbenv-default-gems"

# Load RbEnv into the environment.
PATH_append "$RBENV_ROOT/bin"

eval "$(rbenv init -)"
