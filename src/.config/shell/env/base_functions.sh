source_if_exists(){ [[ ! -f "$1" ]] || source "$1"; }
shell_is_interactive(){ if [[ $- != *i* ]]; then return 0; else return 1; fi; }

which_shell(){ echo "$(basename $SHELL)"; }

which_shellrc(){
  case "$(which_shell)" in
    bash)  echo "$HOME/.bashrc" ;;
    zsh)   echo "$HOME/.zshrc" ;;
  esac
}

touchdir(){ [[ -e "$1" ]] || mkdir -p "$1" 2>/dev/null >&1 || true ;}
touch_user_dir(){ touchdir "$HOME/$1" ;}

touch_default_dirs(){
  # Creates all of the standard files/directories/etc that I need. Operations here should be quick!
  touchdir "$XDG_CONFIG_HOME"
  touchdir "$XDG_DATA_HOME"
  touchdir "$XDG_CACHE_HOME"

  touchdir "$GOPATH"

  touch_user_dir "Notes"
  touch_user_dir "Projects"
  touch_user_dir "Scratch"
}; touch_default_dirs; unset touch_default_dirs

is_linux(){ [[ "$(uname -s)" == Linux* ]] ;}
is_darwin(){ [[ "$(uname -s)" == Darwin* ]] ;}
is_osx(){ is_darwin ;}

PATH_ls(){      echo $PATH | tr -s ":" "\n" ;}
PATH_exists(){  if [[ ! ":$PATH:" == *:$1:* ]]; then echo 0; else echo 1; fi ;}
PATH_append(){  if [[ ! ":$PATH:" == *:$1:* ]]; then export PATH="$PATH:$1"; fi ;}
PATH_prepend(){ if [[ ! ":$PATH:" == *:$1:* ]]; then export PATH="$1:$PATH"; fi ;}
