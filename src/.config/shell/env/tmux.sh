export TMUX_DIR="$HOME/.config/tmux"
export TMUX_PLUGIN_DIR="$TMUX_DIR/plugins"
export TMUX_CONFIG_DIR="$TMUX_DIR/configs"
export TMUX_CONFIG="default"

export TMUXP_CONFIGDIR="$HOME/.config/tmuxp"
