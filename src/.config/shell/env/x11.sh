[[ "$XDG_SSSION_TYPE" == x11 ]] || return

[[ "$XDG_CURRENT_DESKTOP" == "KDE" ]] \
  || export QT_QPA_PLATFORMTHEME="qt5ct"
