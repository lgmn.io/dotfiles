# Dotfiles
export DOTFILES_ROOT="$HOME/.dotfiles"

# X11 :: User Directories
export XDG_RUNTIME_DIR="/run/user/$UID"
export XDG_CACHE_DIR="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

# Common :: Environment Variables.
export BROWSER='firefox'
export EDITOR='nvim'
export PAGER='less'

# Less Pager Configuration.
#default:    export LESSOPEN="|lesspipe %s"
#don't like: export LESSOPEN="| /usr/bin/src-hilite-lesspipe.sh %s"
#default:    export LESS=" -R -M --shift 5"
export LESS=" -qM -R -i -J -F g -c "
export LESSCOLOR=always
#export LESSCOLORIZER=/bin/src-hilite-lesspipe.sh

# ZSH Environment Variables
export ZSH_DIR="$HOME/.config/zsh"

# TODO CLI.
export TODO_CFG_FILE="$XDG_CONFIG_HOME/todo-cli/config"
