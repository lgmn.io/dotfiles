# Grep Aliases / Helpers / Extensions
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

alias kk='clear'
alias KK='reset && clear'

# Disk Usage/Inspection
alias df='df --human-readable'
alias df-most-full='df | grep -Ev "^Filesystem" | sort -nrk5'

# Memory Usage/Inspection
alias free='free -h'

# Network Diagnostics
alias netstat='sudo netstat'
alias netstat-by-pid='sudo netstat -tulanp | grep -Ev "^(Proto|Active)" | sort -k6'

# Listing files & directories.
alias ls='ls -AG'
alias ll='ls -l'               # "list all"
alias lsdf='ls -lda -- .*'      # "list dotfiles"
alias lsd='ls -ld -- *(/)'      # "list directories"
alias mkdir='mkdir -p'
alias rm='rm -f'

# Changing directories quickly.
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

# Environment Variables Manipulation.
alias lsX11env='env | grep -E "^XAUTHORITY|XDG_"'

alias cpv='rsync -pogbr -hhh --backup-dir=/tmp/rsync -e /dev/null --progress'
alias chown-me='sudo chown -R ${USER}:${USER}'
alias rm-broken-symlinks="find -L . -maxdepth 1 -mindepth 1 -type l -delete"

# Editors
alias e='$EDITOR'
alias edit='$EDITOR'
# alias emacs='$EDITOR'
# alias nano='$EDITOR'
# alias vi='$EDITOR'
# alias vim='$EDITOR'

