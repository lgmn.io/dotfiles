which tmux 2>/dev/null >&2 || return

alias tmux='my_tmux'
alias tx='my_tmux'
alias txa='my_tmux attach-session -t'
alias txad='my_tmux attach-session -dt'
alias txks='my_tmux kill-session -t'
alias txlock='my_tmux lock-session -t'
alias txls='my_tmux list-sessions'
alias txls_clients='my_tmux list-clients'
alias txls_cmds='my_tmux list-commands'
alias txrename='my_tmux rename-session -t'
alias txse='my_tmux has-session -t'
alias txunlock='my_tmux unlock-session -t'
