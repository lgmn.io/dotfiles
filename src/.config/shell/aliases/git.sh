# GIT Version Control {{{1

# Use GitHub's `hub` CLI if installed.
if which hub 2>/dev/null >&2; then
  alias git='hub'
  alias g='hub'
else
  alias git='git'
  alias g='git'
fi

alias mkgitdir='f(){ mkdir -p "$1" && touch "$1/.gitignore" ;}; f'
alias gitupdir='for d in $(find . -type d -maxdepth 1 -mindepth 1); do cd "$d"; git up; cd ".."; done'

git_set_fork(){ # Usage: git set-fork [orgname]
  target_org=${1:-$USER}
  current_repo=$(git remote -v | grep '^origin' | head -n1)
  current_org=$(echo -n $current_repo | awk -F':' '{print $2}' | awk -F'/' '{print $1}')
  new_repo=$(echo -n $current_repo | sed "0,/$current_org/{s/$current_org/$target_org/}" | awk '{print $2}')
  git remote rm $target_org 2>/dev/null || true
  git remote add $target_org $new_repo
}

## Git Aliases (Cuz I screw up spaces a lot...)
for a in $(git list-aliases 2>/dev/null | cut -d'=' -f1 ); do
  eval "alias g$a=\"git `echo $a | sed 's/^g//'`\""
done

function my-git-clone() {
  local repo_url="$1"
  local repo_path="$2"

  mkdir -p "$repo_path" 2>/dev/null || true
  cd "$repo_path"

  if ! test -d ".git"; then
    git clone "$repo_url" . 
  else 
    git pull --autostash --ff-only
  fi
}

function r7-clone() {   my-git-clone "git@github.com:rapid7/${1}.git"   "$MY_WORK_DIR/${1}"  ;}
function gh-clone() {   my-git-clone "git@github.com:${1}.git"          "$MY_SCRATCH_DIR/${1}"  ;}
function gl-clone() {   my-git-clone "git@gitlab.com:${1}.git"          "$MY_SCRATCH_DIR/${1}"  ;}
function proj-clone() { my-git-clone "git@github.com:${1}.git"          "$MY_PROJECT_DIR/${1}"  ;}
