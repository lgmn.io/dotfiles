alias edit-vimrc='cd $DOTFILES_ROOT/src/.vim; vim ./.'
alias edit-zshrc='cd $DOTFILES_ROOT/src; vim ./.zshrc'
alias edit-sshconfig='cd $DOTFILES_ROOT/src/.ssh; vim ./config'
alias edit-shell-aliases='cd $DOTFILES_ROOT/src/.config/shell/aliases; vim .'
alias edit-shell-env='cd $DOTFILES_ROOT/src/.config/shell/env; vim .'
alias reload-shell='cd $DOTFILES_ROOT/src/.config/shell/; source load_shell.sh'

