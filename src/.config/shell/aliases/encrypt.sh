# GnuPG
alias gpg-search-mit='gpg --keyserver pgp.mit.edu  --search-keys'
alias gpg-refresh-mit='gpg --keyserver pgp.mit.edu  --refresh-keys'
alias gpg-export-pubkey="gpg --output $HOME/.gnupg/default-public.key --armor --export $MY_GPG_KEY"
alias gpg-export-private="gpg --output $HOME/.gnupg/default-secret.key --armor --export-secret-key $MY_GPG_KEY"

# SSL / TLS / Encryption 
alias ssl-verify='openssl verify -verbose'
alias ssl-view-cert='openssl x509 -text -noout -in'

convert_newline_to_literal(){ awk '{printf "%s\\n", $0}' | head -c -2 ;}

ssh_forget_host(){ sed -i "$1d" $HOME/.ssh/known_hosts ;}
ssl_fetch_cert(){  #USAGE: 'ssl-fetch-cert google.com' or 'ssl-fetch-cert google.com:443'
    local target=$1
    local output="$(echo -n $1 | tr ':' '\n' | head -n1).pem"

    openssl s_client -showcerts -connect $target </dev/null 2>/dev/null \
           | openssl x509 -outform PEM >${output}
}


if `which pass 2>/dev/null >&2`; then 
  alias passenv='_f(){ eval $(pass show env/${1}-${2}) ;}; _f'
fi

