# Ruby / Bundler / Chef / ...
alias bx='bundle exec'

alias json2yaml="ruby -ryaml -rjson -e 'puts YAML.dump(JSON.parse(STDIN.read))'"
alias yaml2json="ruby -ryaml -rjson -e 'puts JSON.dump(YAML.parse(STDIN.read))'"

# Chef Knife
alias knife_search='knife search -i'
alias knife_attr='knife node attribute'
alias knife_attrs='_(){ knife node attribute get -F json "$@" | jq "." ;}; _'
alias knife_edit='knife node edit'

# ChefDK
alias cexec='chef exec'
alias ckitchen='chef exec kitchen'
alias cknife='chef exec kitchen'
