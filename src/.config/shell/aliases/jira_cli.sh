# JIRA  (github.com/Netflix-Skunkworks/go-jira) 
which jira 2>/dev/null >&2 || return

alias j='jira'
alias jls='jira ls'
alias jls-priority='jira ls -s "Sprint,\"Global Rank\",updatedDate DESC"'
alias jv='jira view'
alias jdone='jira done --edit'
alias jcomment='jira comment'
alias jassign='jira assign'
alias jclose='jira close --edit'
alias jresolve='jira resolve --edit'
alias jnew='jira create --issuetype=Story'
alias jnew-story='jira create --issuetype=Story'
alias jnew-bug='jira create --issuetype=Bug'
alias jnew-releasestory='jira create --issuetype="Release Story"'
alias jnew-changestory='jira create --issuetype="Change Story"'

