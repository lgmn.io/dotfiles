[[ `uname -s` == Linux* ]] || return

alias dmesg='sudo dmesg -H'
alias dpkg='sudo dpkg'

# Kernel Aliases {{{1
kern_list_modules(){
  {
    printf "NAME                   ?????  ?   DEPENDENCIES\n"
    printf "----------------------------------------------\n"
    sudo lsmod | sort
  } | ${PAGER:-less}
}

kern_print_install_date(){
  local d=$(ls -tcd --full-time "/lost+found" | awk '{printf $6 " " $7}')
  echo "System Install Date: "
  echo -n " - Date: "; date --date="$d" +"%x"
  echo -n " - Time: "; date --date="$d" +"%X"
}
