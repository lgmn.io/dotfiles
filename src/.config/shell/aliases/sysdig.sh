which sysdig 2>/dev/null >&2 || return

## SysDig 
alias sysdig='sudo sysdig -pc'
alias csysdig='sudo csysdig -pc'
alias sysdig-opensnoop='sysdig -p "%12user.name %6proc.pid %12proc.name %3fd.num %fd.typechar %fd.name" evt.type=open'
alias sysdig-openetc='sysdig evt.type=open and fd.name contains /etc'
alias sysdig-watchssh='sysdig -A -c echo_fds fd.name=/dev/ptmx and proc.name=sshd'
alias sysdig-topfailedcalls='sysdig -c topscalls "evt.failed=true"'
sysdig_capall(){ sudo sysdig -qw ${1:-$(date -I).scap} ;}
