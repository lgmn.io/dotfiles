which docker 2>/dev/null >&2 || return

# Docker 
alias d='docker'
alias dc='docker-compose'
alias docker-ps-ports='docker ps --format "table {{ .Names }}\t{{ .Status }}\t{{ .Ports }}"'
