which kubectl 2>/dev/null >&2 || return

# Kubernetes 
alias mk='minikube'
alias k='kubectl'
alias kgetall='kubectl get all --all-namespaces'
alias ky='kubectl -o yaml'
alias kj='kubectl -o json'
alias kw='kubectl -o wide'
alias ked='kubectl -o json edit'
alias kex='kubectl explain'
alias ka='kubectl apply --record -Rf'
alias kd='kubectl describe'

