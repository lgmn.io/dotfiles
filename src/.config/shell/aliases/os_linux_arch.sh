which pacman 2>/dev/null >&2 || return

# Package Management {{{1
alias pacman-pkg-info="pacman -Qi"
alias pacman-pkg-changelog="pacman -Qc"

# Synchronize with repositories and then upgrade packages that are out of date on the local system.
alias pacupg='sudo pacman -Syu'
# Download specified package(s) as .tar.xz ball
alias pacdl='pacman -Sw'
# Install specific package(s) from the repositories
alias pacin='sudo pacman -S'
# Install specific package not from the repositories but from a file
alias pacins='sudo pacman -U'
# Remove the specified package(s), retaining its configuration(s) and required dependencies
alias pacre='sudo pacman -R'
# Remove the specified package(s), its configuration(s) and unneeded dependencies
alias pacrem='sudo pacman -Rns'
# Display information about a given package in the repositories
alias pacrep='pacman -Si'
# Search for package(s) in the repositories
alias pacreps='pacman -Ss'
# Display information about a given package in the local database
alias pacloc='pacman -Qi'
# Search for package(s) in the local database
alias paclocs='pacman -Qs'
# List all packages which are orphaned
alias paclo='pacman -Qdt'
# Clean cache - delete all the package files in the cache
alias pacc='sudo pacman -Scc'
# List all files installed by a given package
alias paclf='pacman -Ql'
# Show package(s) owning the specified file(s)
alias pacown='pacman -Qo'
# Mark one or more installed packages as explicitly installed
alias pacexpl='pacman -D --asexp'
# Mark one or more installed packages as non explicitly installed
alias pacimpl='pacman -D --asdep'
# Update and refresh the local package and ABS databases against repositories
alias pacupd='sudo pacman -Sy && sudo abs'
# Install given package(s) as dependencies
alias pacinsd='sudo pacman -S --asdeps'
# Force refresh of all package lists after updating /etc/pacman.d/mirrorlist
alias pacmir='sudo pacman -Syy'
# remove packages that aren't needed (and not installed directly)
alias pacrmnotneeded='sudo pacman -Rscn $(pacman -Qdtq)'
# logs
alias paclog='tail -n 40 /var/log/pacman.log'
# list packages installed from AUR
alias paclistaur='pacman -Qemq'

# Service Management {{{1
alias pacman-upgrade-system="yaourt -Syu"
alias pacman-install-logs="egrep '\\[.*\\] \\[.*\\] installed' /var/log/pacman.log | sed 's/\\[ALPM\\]//'"
alias pacman-upgrade-logs="egrep '\\[.*\\] \\[.*\\] upgraded' /var/log/pacman.log | sed 's/\\[ALPM\\]//'"
alias systemctl-user-list-files="systemctl --user list-unit-files | sed '1d' | grep -v 'unit files listed.'"

alias systemctl='systemctl --no-legend --no-ask-password -o cat --lines=1000'
alias journalctl='journalctl'

