"Functions {{{1
function! s:IniFold(lnum) "{{{
    let line = getline(a:lnum)

    if line =~ '^\s*\[[^]]*\]'
        return '>1'
    endif

    return '='
endfunction

function! s:IniFoldActivate() "{{{
    setlocal foldmethod=expr
    setlocal foldexpr=IniFold(v:lnum)
endfunction

function! s:IniFoldUndo() "{{{
    if exists('b:undo_ftplugin')
      let b:undo_ftplugin .= "|setl foldexpr< foldmethod<"
    else
      let b:undo_ftplugin = "setl foldexpr< foldmethod<"
    endif
endfunction

" Bind DOSini folding functions {{{1
call s:IniFoldActivate()
