au BufNewFile,BufRead *.log         setl filetype=log
au BufNewFile,BufRead *.sls         setl filetype=yaml
au BufNewFile,BufRead *.Dockerfile  setl filetype=dockerfile
au BufNewFile,BufRead Dockerfile.*  setl filetype=dockerfile
au BufNewFile,BufRead *.envrc*      setl filetype=sh
au BufNewFile,BufRead *.tfstate     setl filetype=json
au BufNewFile,BufRead *.tfstate*    setl filetype=json
au BufNewFile,BufRead zshrc.local   setl filetype=zsh
au BufNewFile,BufRead vimrc.local   setl filetype=vim

au! BufNewFile,BufRead *.yaml,*.yml call s:set_yaml_filetype()
function! s:set_yaml_filetype()
  let hasApiVersion = match(readfile(expand("%:p")), "apiVersion:") != -1
  if hasApiVersion
    setfiletype yaml.k8s
  else
    setfiletype yaml
  endif
endfunction
