## Initialize ZSH Plugins
## ====================================================================
export ZSH_DIR="${ZSH_DIR:-$HOME/.zsh}"
source "${ZSH_DIR}/lib/zgen/zgen.zsh"

function zgen_rebuild_cache(){
  [[ ! -d "$HOME/.zgen" ]] || rm -rf "$HOME/.zgen"
  zgen_build_cache
}

function zgen_build_cache(){

  # If a cache already exists, dont overwrite it.
  if zgen saved; then return 0; fi

  [[ -d "$HOME/.zgen" ]] || mkdir -p "$HOME/.zgen"

  zgen oh-my-zsh

  # Common linux utils (always on).
  zgen oh-my-zsh "plugins/colored-man-pages"
  zgen oh-my-zsh "plugins/command-not-found"
  zgen oh-my-zsh "plugins/compleat"
  zgen oh-my-zsh "plugins/gnu-utils"
  # zgen oh-my-zsh "plugins/gpg-agent"
  zgen oh-my-zsh "plugins/man"
  zgen oh-my-zsh "plugins/pass"
  zgen oh-my-zsh "plugins/pip"
  zgen oh-my-zsh "plugins/rsync"
  zgen oh-my-zsh "plugins/sudo"

  if test -x "$(which git 2>/dev/null)"; then
    zgen oh-my-zsh "plugins/git"
    zgen oh-my-zsh "plugins/git-extras"
    zgen oh-my-zsh "plugins/git-flow-avh"
  fi

  zgen load "zsh-users/zsh-syntax-highlighting"

  # Load custom plugins (always run last to get precedence).
  zgen load      "${ZSH_DIR}/lib/plugins/jlogemann"

  # Theme
  zgen oh-my-zsh "themes/miloshadzic"

  # Write the configuration file to disk.
  zgen save
}

zgen_build_cache
