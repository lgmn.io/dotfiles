autoload -Uz compinit && compinit

# compdef k=kubectl
# compdef tf=terraform

compdef todo=todo.sh
compdef g=git
compdef d=docker
compdef vg=vagrant
compdef _files cpv

