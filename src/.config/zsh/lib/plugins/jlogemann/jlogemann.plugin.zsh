## Aliases
## ====================================================================
alias zgen-reload="zgen-reset; clear; reset; source $HOME/.zshrc"

# Edit the current command line in $EDITOR
autoload -U edit-command-line
zle -N edit-command-line
bindkey '\C-x\C-e' edit-command-line

## Telemetry
# -----------
# I am constantly adding and removing aliases. This is useful for
# figuring out which aliases and commands I should invest more
# effort into automating.
function __my_zsh_most_used_cmds(){ history ;}
function __my_zsh_most_used_bins(){ ;}

function zsh-telemetry() {
  local count="${1:-25}"
  echo -e "\n** Most Used Commands (${count}) :: \n"
  history \
    | awk '{print $2,$3,$4,$5,$6}' \
    | uniq -c \
    | sort -rn \
    | awk '$1 > 1 {print ;}' \
    | tail -n ${count}

  echo -e "\n** Most Used Binaries (${count}) ::"
  history \
    | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' \
    | grep -v "./" \
    | column -c3 -s " " -t \
    | sort -nr \
    | head -n ${count} \
    | awk '{print $1,$3," (" $2 ")"}' \
    | column -t \
    | sed 's/^/  /' 
}


## ZSH Configuration
# -------------------
# Always source global ZSH configs and local ones (/etc/zshrc, ~/.zshrc).
setopt GLOBAL_RCS RCS

# Allow comments even in interactive shells.
setopt INTERACTIVE_COMMENTS   #(-k) <K> <S>

# Treat the ‘#’, ‘~’ and ‘^’ characters as part of patterns for filename
# generation, etc. (An initial unquoted ‘~’ always produces named directory
# expansion.)
setopt EXTENDED_GLOB

## Job Control
# -------------
# Run all background jobs at a lower priority. This option is set by default.
setopt BG_NICE  #(-6) <C> <Z>

# With this option set, stopped jobs that are removed from the job table with
# the disown builtin command are automatically sent a CONT signal to make them
# running.
setopt AUTO_CONTINUE

# Treat single word simple commands without redirection as candidates for
# resumption of an existing job.
setopt AUTO_RESUME  #(-W)

## Auto-completion / Aliases
# ---------------------------
# Expand aliases.
setopt ALIASES  #<D>

# Prevents aliases on the command line from being internally substituted
# before completion is attempted. The effect is to make the alias a distinct
# command for completion purposes.
setopt COMPLETE_ALIASES

# Try to make the completion list smaller (occupying less lines) by printing
# the matches in columns with different widths.
setopt LIST_PACKED

# When listing files that are possible completions, show the type of each file
# with a trailing identifying mark.
setopt LIST_TYPES

# This option works when AUTO_LIST or BASH_AUTO_LIST is also set. If there is an
# unambiguous prefix to insert on the command line, that is done without a
# completion list being displayed; in other words, auto-listing behaviour only
# takes place when nothing would be inserted. In the case of BASH_AUTO_LIST, this
# means that the list will be delayed to the third call of the function.
setopt LIST_AMBIGUOUS

# Use cache for cmds/dirs (and attempt to correct spelling errors).
setopt CORRECT HASH_DIRS HASH_CMDS HASH_LIST_ALL

## Directory Navigation
# ----------------------
# Append a trailing ‘/’ to all directory names resulting from filename
# generation (globbing).
setopt MARK_DIRS   #(-8, ksh: -X)

# If the argument to a cd command (or an implied cd with the AUTO_CD option
# set) is not a directory, and does not begin with a slash, try to expand the
# expression as if it were preceded by a ‘~’ (see Filename Expansion).
setopt CDABLE_VARS

# Don’t push multiple copies of the same directory onto the directory stack.
setopt PUSHD_IGNORE_DUPS

# Do not print the directory stack after pushd or popd.
setopt PUSHD_SILENT   #(-E)

# Have pushd with no arguments act like ‘pushd $HOME’.
setopt PUSHD_TO_HOME  #(-D)

## Shell History 
# ---------------
# Perform textual history expansion, csh-style, treating the character ‘!’
# specially.
setopt BANG_HIST   #(+K) <C> <Z>

# Remove superfluous blanks from each command line being added to the history
# list.
setopt HIST_REDUCE_BLANKS

# If a new command line being added to the history list duplicates an older
# one, the older command is removed from the list (even if it is not the
# previous event)
# setopt HIST_IGNORE_ALL_DUPS

# When writing out the history file, older commands that duplicate newer ones
# are omitted.
setopt HIST_SAVE_NO_DUPS

# This option both imports new commands from the history file, and also
# causes your typed commands to be appended to the history file (the
# latter is like specifying INC_APPEND_HISTORY, which should be turned off
# if this option is in effect). The history lines are also output with
# timestamps ala EXTENDED_HISTORY (which makes it easier to find the spot
# where we left off reading the file after it gets re-written).  By
# default, history movement commands visit the imported lines as well as
# the local lines, but you can toggle this on and off with the
# set-local-history zle binding. It is also possible to create a zle
# widget that will make some commands ignore imported commands, and some
# include them.  If you find that you want more control over when commands
# get imported, you may wish to turn SHARE_HISTORY off, INC_APPEND_HISTORY
# or INC_APPEND_HISTORY_TIME (see above) on, and then manually import
# commands whenever you need them using ‘fc -RI’.
setopt SHARE_HISTORY  #<K>

# Whenever the user enters a line with history expansion, don’t execute the
# line directly; instead, perform history expansion and reload the line into
# the editing buffer.
setopt HIST_VERIFY

# If this is set, zsh sessions will append their history list to the history
# file, rather than replace it. Thus, multiple parallel zsh sessions will all
# have the new entries from their history lists added to the history file, in the
# order that they exit. The file will still be periodically re-written to trim it
# when the number of lines grows 20% beyond the value specified by $SAVEHIST (see
# also the HIST_SAVE_BY_COPY option).
setopt APPEND_HISTORY

# This options works like APPEND_HISTORY except that new history lines are
# added to the $HISTFILE incrementally (as soon as they are entered), rather
# than waiting until the shell exits. The file will still be periodically
# re-written to trim it when the number of lines grows 20% beyond the value
# specified by $SAVEHIST (see also the HIST_SAVE_BY_COPY option).
setopt INC_APPEND_HISTORY

## Local Configuration Overrides 
# -------------------------------
# Ensure a local file exists for ZSH overrides (touch confuses open vim buffers).
test -e "$HOME/.zshrc.local" || touch "$HOME/.zshrc.local"
