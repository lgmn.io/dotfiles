# Jake's Dotfiles

### Install
```bash

git clone --recursive "git@gitlab.com:JakeLogemann/dotfiles.git" ~/.dotfiles \
  && cd ~/.dotfiles

# Simulate the install.
make check

# Force install (overwrite existing files) w/ GNU Stow.
make force-install
```
